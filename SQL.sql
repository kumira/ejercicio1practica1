/* miriam */

DROP DATABASE IF EXISTS pr1u2mod2ej1;

CREATE DATABASE pr1u2mod2ej1;

USE pr1u2mod2ej1;

CREATE TABLE Tipo_de_habitacion(
  IdTipo int,
  Categoria varchar(20),
  PRIMARY KEY (IdTipo)
);

CREATE TABLE Habitacion(
  NumHabitacion int,
  IdTipo int,
  PRIMARY KEY (Numhabitacion),
  CONSTRAINT habitaciontipohab FOREIGN KEY (IdTipo) REFERENCES Tipo_de_habitacion (IdTipo)
  
);

CREATE TABLE Clientes(
  DNI varchar (15),
  nombre varchar (50),
  apellidos varchar (70),
  telefono int,
  email varchar (50),
  direccion varchar (100),
  FechaNac date,
  Poblacion varchar (50),
  codpostal int,
  provincia varchar (50),
  pais varchar (20),
  PRIMARY KEY (DNI)
  
);

CREATE TABLE Reserva(
  CodigoReserva int,
  Fechaentrada date,
  FechaSalida date,
  NumHabitacion int, 
  DNI varchar (15),
  IVA int,
  PRIMARY KEY (CodigoReserva),
  CONSTRAINT reservanumhab FOREIGN KEY (NumHabitacion) REFERENCES Habitacion (NumHabitacion),
  CONSTRAINT reservaclientes FOREIGN KEY (DNI) REFERENCES Clientes (DNI)

);

CREATE TABLE gastos(
  CodigoReserva int,
  IdGasto int,
  DescGasto int,
  ImporteGasto int,
  PRIMARY KEY (CodigoReserva,IdGasto),
  CONSTRAINT gastosreserva FOREIGN KEY (CodigoReserva) REFERENCES Reserva (CodigoReserva)
  
);

