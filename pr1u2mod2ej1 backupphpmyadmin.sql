-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-03-2018 a las 11:08:20
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pr1u2mod2ej1`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `DNI` varchar(15) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellidos` varchar(70) DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `FechaNac` date DEFAULT NULL,
  `Poblacion` varchar(50) DEFAULT NULL,
  `codpostal` int(11) DEFAULT NULL,
  `provincia` varchar(50) DEFAULT NULL,
  `pais` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

CREATE TABLE `gastos` (
  `CodigoReserva` int(11) NOT NULL,
  `IdGasto` int(11) NOT NULL,
  `DescGasto` int(11) DEFAULT NULL,
  `ImporteGasto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitacion`
--

CREATE TABLE `habitacion` (
  `NumHabitacion` int(11) NOT NULL,
  `IdTipo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva`
--

CREATE TABLE `reserva` (
  `CodigoReserva` int(11) NOT NULL,
  `Fechaentrada` date DEFAULT NULL,
  `FechaSalida` date DEFAULT NULL,
  `NumHabitacion` int(11) DEFAULT NULL,
  `DNI` varchar(15) DEFAULT NULL,
  `IVA` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_de_habitacion`
--

CREATE TABLE `tipo_de_habitacion` (
  `IdTipo` int(11) NOT NULL,
  `Categoria` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`DNI`);

--
-- Indices de la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD PRIMARY KEY (`CodigoReserva`,`IdGasto`);

--
-- Indices de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  ADD PRIMARY KEY (`NumHabitacion`),
  ADD KEY `habitaciontipohab` (`IdTipo`);

--
-- Indices de la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`CodigoReserva`),
  ADD KEY `reservanumhab` (`NumHabitacion`),
  ADD KEY `reservaclientes` (`DNI`);

--
-- Indices de la tabla `tipo_de_habitacion`
--
ALTER TABLE `tipo_de_habitacion`
  ADD PRIMARY KEY (`IdTipo`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD CONSTRAINT `gastosreserva` FOREIGN KEY (`CodigoReserva`) REFERENCES `reserva` (`CodigoReserva`);

--
-- Filtros para la tabla `habitacion`
--
ALTER TABLE `habitacion`
  ADD CONSTRAINT `habitaciontipohab` FOREIGN KEY (`IdTipo`) REFERENCES `tipo_de_habitacion` (`IdTipo`);

--
-- Filtros para la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD CONSTRAINT `reservaclientes` FOREIGN KEY (`DNI`) REFERENCES `clientes` (`DNI`),
  ADD CONSTRAINT `reservanumhab` FOREIGN KEY (`NumHabitacion`) REFERENCES `habitacion` (`NumHabitacion`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
